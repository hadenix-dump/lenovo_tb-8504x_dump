#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:37862700:8c5b6f084134f198048682ea08b32d93221af8ef; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:32953640:d1c78677c9a59fba90df37ab09178180518a15b2 EMMC:/dev/block/bootdevice/by-name/recovery 8c5b6f084134f198048682ea08b32d93221af8ef 37862700 d1c78677c9a59fba90df37ab09178180518a15b2:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
