## msm8937_64-user 8.1.0 OPM1.171019.019 7 release-keys
- Manufacturer: lenovo
- Platform: msm8937
- Codename: TB-8504X
- Brand: Lenovo
- Flavor: msm8937_64-user
- Release Version: 8.1.0
- Id: OPM1.171019.019
- Incremental: 8504X_S001031_191204_ROW
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Fingerprint: Lenovo/TB-8504X/TB-8504X:8.1.0/OPM1.171019.019/8504X_S001031_191204_ROW:user/release-keys
- Branch: msm8937_64-user-8.1.0-OPM1.171019.019-7-release-keys